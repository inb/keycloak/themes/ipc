function openTermsOfUse() {
     $('#modalTerms').modal({ show: 'true' });

     $.ajax({
       type: "POST",
       url: "https://vre.multiscalegenomics.eu/applib/getTermsOfUse.php",
       data:"id=1",
       success: function(data) {
        $('#modalTerms .modal-body .container-terms').html(data);
      }
    });
  }
